import "package:planetary_gears/src/model.dart";
import "package:test/test.dart";

void main() {
  // all the tests that we should pass: http://john-s-allen.com/gears/hubratios.htm
  group("single stage ratios", () {
    test("single set non-planetary gear", () {
      final sets = [
        new PlanetSet.canonical(48, 24),
      ];
      final paths = [
        new GearPath(
          input: new Shaft.RING(0),
          output: new Shaft.SUN(0),
          fixed: const Shaft.CARRIER(),
        )
      ];
      final stage = new GearStage(sets, paths);
      expect(stage.ratio(stage.paths.first), closeTo(2, 0.00001));
    });

    test("double set non-planetary gear", () {
      final sets = [
        new PlanetSet(48, null, 12),
        new PlanetSet(null, 20, 16),
      ];
      final paths = [
        new GearPath(
          input: new Shaft.RING(0),
          output: new Shaft.SUN(1),
          fixed: const Shaft.CARRIER(),
        )
      ];
      final stage = new GearStage(sets, paths);
      expect(stage.ratio(stage.paths.first), closeTo(3.2, 0.00001));
    });

    test("the classic double speed", () {
      final set = new PlanetSet.canonical(48, 16);
      final stage = new GearStage([set], [GearPath.singleFixedSunUp]);
      expect(stage.ratio(stage.paths.first), closeTo(1.3333, 0.0001));
    });

    test("3-speed wide", () {
      final set = new PlanetSet.canonical(48, 24);
      final paths = [
        GearPath.singleFixedSunDown,
        GearPath.directDrive,
        GearPath.singleFixedSunUp
      ];
      final stage = new GearStage([set], paths);

      var actual = stage.ratios().toList();
      expect(actual.length, 3);
      expect(actual[0], closeTo(0.66666666, 0.00001));
      expect(actual[1], closeTo(1, 0.00001));
      expect(actual[2], closeTo(1.5, 0.00001));
    });

    test("Nexus Inter-4", () {
      final sets = [
        new PlanetSet(null, 30, 30),
        new PlanetSet(78, 39, 19),
        new PlanetSet(null, 45, 13),
      ];
      var paths = [
        GearPath.directDrive,
        new GearPath.carrierToRingFixedSun(sets, 0),
        new GearPath.carrierToRingFixedSun(sets, 1),
        new GearPath.carrierToRingFixedSun(sets, 2),
      ];
      final stage = new GearStage(sets, paths);

      var actual = stage.ratios().toList();
      expect(actual.length, 4);
      expect(actual[0], closeTo(1, 0.00001));
      expect(actual[1], closeTo(1.24359, 0.00001));
      expect(actual[2], closeTo(1.500, 0.00001));
      expect(actual[3], closeTo(1.84319, 0.00001));
    });

    test("Sturmey Archer TC", () {
      // tooth counts here: https://hadland.files.wordpress.com/2012/07/2tables.pdf
      // architecture derived knowing the ratio from here: http://john-s-allen.com/gears/hubratios.htm
      final sets = [
        new PlanetSet(null, 15, 25),
        new PlanetSet(54, null, 14),
      ];
      var paths = [
        new GearPath(
          input: const Shaft.RING(1),
          output: const Shaft.CARRIER(),
          fixed: const Shaft.SUN(0),
        ),
      ];
      final stage = new GearStage(sets, paths);

      expect(stage.ratio(paths[0]), closeTo(0.865, 0.001));
    });

    test("Sturmey Archer FW", () {
      // very early 4-gear hub which already uses the arc principles of later
      // 5-gear hubs. source: https://hadland.files.wordpress.com/2012/07/1theory.pdf
      // for fun, read 1939's paper: http://www.sturmey-archerheritage.com/images/photos/pic-502.jpg
      // Sturmey Archer Nomenclature: W - wide (gear steps), M - medium, R – close steps
      // F - four gear: AF - "close range", FM and FW as per above convention.
      final sets = [
        new PlanetSet(60, 30, 14),
        new PlanetSet(null, 24, 21),
      ];
      var paths = [
        new GearPath(
          input: const Shaft.RING(0),
          output: const Shaft.CARRIER(),
          fixed: const Shaft.SUN(0),
        ),
        new GearPath(
          input: const Shaft.RING(0),
          output: const Shaft.CARRIER(),
          fixed: const Shaft.SUN(1),
        ),
        GearPath.directDrive,
        new GearPath(
          input: const Shaft.CARRIER(),
          output: const Shaft.RING(0),
          fixed: const Shaft.SUN(1),
        ),
      ];
      final stage = new GearStage(sets, paths);

      var actual = stage.ratios().toList();
      expect(actual.length, 4);
      expect(actual[0], closeTo(0.666, 0.001));
      expect(actual[1], closeTo(0.789, 0.001));
      expect(actual[2], closeTo(1.0, 0.001));
      expect(actual[3], closeTo(1.266, 0.001));
    });

    test("Single Sun, three speeds, hypothetical", () {
      final sets = [
        new PlanetSet(78, 39, 19),
        new PlanetSet(69, null, 10),
      ];
      var paths = [
        GearPath.directDrive,
        new GearPath(
          input: const Shaft.RING(0),
          output: const Shaft.RING(1),
          fixed: const Shaft.SUN(0),
        ),
        GearPath.singleFixedSunDown,
      ];
      final stage = new GearStage(sets, paths);

      var actual = stage.ratios().toList();
      expect(actual.length, 3);
      expect(actual[0], closeTo(1.00, 0.0001));
      expect(actual[1], closeTo(0.8649, 0.0001));
      expect(actual[2], closeTo(0.6666, 0.0001));
    });
  });
}
