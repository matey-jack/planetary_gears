import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

import 'src/gear_stage/gear_stage_component.dart';
import 'src/gearbox_drawing.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [materialDirectives, GearStageComponent, GearboxDrawing],
  providers: const [materialProviders],
)
class AppComponent {
  // Nothing here yet. All logic is in TodoListComponent.
}
