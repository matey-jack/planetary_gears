import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import '../model.dart';

@Component(
  selector: 'gear_set',
  styleUrls: const ['gear_set_component.css'],
  templateUrl: 'gear_set_component.html',
  directives: const [
    CORE_DIRECTIVES,
    formDirectives,
    materialDirectives,
  ],
)
// TODO: for consistency rename to PlanetSetComponent
class GearSetComponent implements OnInit, OnChanges {
  final _updates = new StreamController<PlanetSet>();

  @Input()
  PlanetSet set;

  @Input()
  int axis;

  @Output()
  Stream<PlanetSet> get update => _updates.stream;

  // numeric input fields in HTML DOM always need doubles due to JavaScript typesystem in use.
  double ring;
  double sun;
  double planets;

  GearSetComponent() {}

  @override
  Future<Null> ngOnInit() async {
  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    for (final s in changes.keys) {
      SimpleChange c = changes[s];
      print("${s}: ${c.previousValue} -> ${c.currentValue.toString()}");
    }

    // we don't get called here when attributes of our set change!
    // luckily we get a notification for the changed axis!
    ring = set.ring.toDouble();
    sun = set.sun.toDouble();
    planets = set.planets.toDouble();
  }

  void ringChanged() {
    set.ring = ring.round();
    _updates.add(set);
  }

  void sunChanged() {
    set.sun = sun.round();
    _updates.add(set);
  }

  void planetsChanged() {
    set.planets = planets.round();
    set.sun = axis - set.planets;
    set.ring = axis + set.planets;
    _updates.add(set);
    ngOnChanges({});
  }

}
