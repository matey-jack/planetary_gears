class PlanetSet {
  int ring;
  int planets;
  int sun;

  PlanetSet(this.ring, this.sun, this.planets) {
    assert(planets != null);
    assert(ring != null || sun != null);
  }

  PlanetSet.canonical(this.ring, this.sun) : planets = (ring - sun) ~/ 2 {
    assert(ring != null || sun != null);
  }
}

int _ringIndex(List<PlanetSet> sets) {
  for (int i = 0; i < sets.length; i++) {
    if (sets[i].ring != null) {
      return i;
    }
  }
  throw "Asking for ring's index, but there is none!";
}

enum ShaftType { RING, SUN, CARRIER }

class Shaft {
  final int index;
  final ShaftType type;

  Shaft(this.type, this.index) {
    assert((type == ShaftType.CARRIER) == (index == null));
    assert(index == null || index > 0);
  }
  const Shaft.CARRIER()
      : type = ShaftType.CARRIER,
        index = null;
  const Shaft.RING(this.index) : type = ShaftType.RING;
  const Shaft.SUN(this.index) : type = ShaftType.SUN;
}

class GearPath {
  final Shaft input, output, fixed;

  GearPath({this.input, this.output, this.fixed}) {
    // input and output must be set
    assert(null != input && null != output);

    // fixed shaft can't be anything else
    assert(fixed != input && fixed != output);
  }

  GearPath.carrierToRingFixedSun(List<PlanetSet> sets, int i)
      : input = const Shaft.CARRIER(),
        output = new Shaft.RING(_ringIndex(sets)),
        fixed = new Shaft.SUN(i);

  static final singleFixedSunUp = new GearPath(
    input: const Shaft.CARRIER(),
    output: const Shaft.RING(0),
    fixed: const Shaft.SUN(0),
  );

  static final singleFixedSunDown = new GearPath(
    input: const Shaft.RING(0),
    output: const Shaft.CARRIER(),
    fixed: const Shaft.SUN(0),
  );

  static final directDrive = new GearPath(
    input: const Shaft.CARRIER(),
    output: const Shaft.CARRIER(),
    fixed: null,
  );

  get _shafts => [input, output, fixed];
  int get sunIndex =>
      _shafts.firstWhere((sh) => sh.type == ShaftType.SUN).index;
  int get ringIndex =>
      _shafts.firstWhere((sh) => sh.type == ShaftType.RING).index;
}

class GearStage {
  final List<PlanetSet> sets;
  final List<GearPath> paths;

  GearStage(this.sets, this.paths) {
    // all indexes in paths are in the range of sets.
    // all shafts in paths are non-null in sets.
    for (int i = 0; i < paths.length; i++) {
      _checkShaft(paths[i].input, "input", i);
      _checkShaft(paths[i].output, "output", i);
      _checkShaft(paths[i].fixed, "fixed", i);
    }
    assert(true);
  }

  void _checkShaft(Shaft shaft, String pathPlace, int pathIndex) {
    if (shaft == null) {
      return;
    }
    final pathName = "Path[${pathIndex}].${pathPlace}";
    if (shaft.index != null &&
        (shaft.index < 0 || shaft.index >= sets.length)) {
      throw new ArgumentError(
          pathName + " refers shaft on non-existing set (${shaft.index}).");
    }
    if (shaft.type == ShaftType.RING && sets[shaft.index].ring == null) {
      throw new ArgumentError(pathName +
          " refers non-existing ring gear in set ${shaft.index}.");
    }
    if (shaft.type == ShaftType.SUN && sets[shaft.index].sun == null) {
      throw new ArgumentError(pathName +
          " refers non-existing sun gear in set ${shaft.index}.");
    }
  }

  Iterable<double> ratios() => paths.map(this.ratio);

  double ratio(GearPath path) {
    if (path.input == path.output) {
      return 1.0;
    }
    if (path.fixed.type == ShaftType.CARRIER) {
      return _getShaftRatio(path.input) / _getShaftRatio(path.output);
    }
    return _ratioToCarrier(path.input, path.fixed) *
        _ratioFromCarrier(path.output, path.fixed);
  }

  double _getShaftRatio(Shaft shaft) {
    var set = sets[shaft.index];
    if (shaft.type == ShaftType.RING) {
      return set.ring / set.planets;
    } else {
      assert(shaft.type == ShaftType.SUN);
      return set.sun / set.planets;
    }
  }

  double _ratioFromCarrier(Shaft input, Shaft fixed) {
    if (input.type == ShaftType.CARRIER) {
      return 1.0;
    }
    return 1 + _getShaftRatio(fixed) / _getShaftRatio(input);
  }

  double _ratioToCarrier(Shaft output, Shaft fixed) =>
      1 / _ratioFromCarrier(output, fixed);
}
