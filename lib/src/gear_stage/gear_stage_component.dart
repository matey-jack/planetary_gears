import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import '../gear_set/gear_set_component.dart';
import '../model.dart';

@Component(
  selector: 'gear_stage',
  styleUrls: const ['gear_stage_component.css'],
  templateUrl: 'gear_stage_component.html',
  directives: const [
    CORE_DIRECTIVES,
    formDirectives,
    GearSetComponent,
  ],
  pipes: const [COMMON_PIPES],
)
class GearStageComponent implements OnChanges {
  int axis = 36;
  List<PlanetSet> sets = [
    new PlanetSet.canonical(48, 24),
    new PlanetSet.canonical(45, 27),
  ];
  GearStage stage;

  GearStageComponent() {
    stage = new GearStage(sets, allPossibleCarrierToRingPaths());
  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    // TODO: implement ngOnChanges
  }

  void update() {
    // create a couple of sensible paths
    stage = new GearStage(sets, allPossibleCarrierToRingPaths());
  }

  void axisChanged() {
    for (final s in sets) {
      s.ring = axis + s.planets;
      s.sun = axis - s.planets;
    }
  }

  List<GearPath> allPossibleCarrierToRingPaths() {
    List<GearPath> paths = [];
    for (int i = 0; i < sets.length; i++) {
      if (sets[i].sun != null && sets[i].sun > 0) {
        for (int r = 0; r < sets.length; r++) {
          if (sets[r].ring != null && sets[r].ring > 0) {
            paths.add(new GearPath(
              input: new Shaft.CARRIER(),
              output: new Shaft.RING(r),
              fixed: new Shaft.SUN(i),
            ));
          }
        }
      }
    }
    return paths;
  }
}
