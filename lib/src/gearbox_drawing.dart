import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
  selector: 'gearbox-drawing',
  styleUrls: const ['gearbox_drawing.css'],
  templateUrl: 'gearbox_drawing.html',
)
class GearboxDrawing implements OnInit {
  String name = 'Angular';

  @override
  ngOnInit() {
    print("Hello from Gearbox Drawing!");
  }
}
