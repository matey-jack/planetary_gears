Build by pub & stagehand.

But also:

    pub global activate angular_cli

New way forward:
 - make it an Angular example project which also works well on mobile
 - no diagrams for now
 - simplified data model of N = one .. three stages with the same base architecture: 
   + `K_n` = one .. four sets, of which each has a sun, only one has a ring.
   + each sets has translates either up (in: carrier, out: ring) or down (in: ring, out: carrier)
   + each set has `K_n` states (which sun is fixed) and for the resulting gears we always show all `K_1 × ... K_N` combinations
 - simplified interface:
   + two-part screen with editing stages and sets above and resulting ratios below
   + above part shows either:
     * arc-view where we can add/remove stages and choose direction (up/down) of stages
     * set-view where we can set tooth counts in each set
     * possibly have an intermediate view where we choose the ring gear of the set and its direction
     ==> goal is to make it touch-friendly! Lower part of screen may be hidden by soft-keyboard, but later we can add a separate mobile UI to adjust the numbers --> something that should be mainstream enough to find how-tos on the net (as opposed to SVG...)
   + resulting ratios will automatically be sorted and show the gear steps and allow disabling redundant combinations via tap on a checkbox
   
Build a minimal backend as soon as above MVP works:
 - use as much of Firebase pre-baked tech as possibly; also things that will later work in Flutter as well
 - Google Login (quickfix for read-only mode: only I have an auth-key)
 - List of models with a few data fields: maker, start/end of production
 - Models are associated with a user and can be forked
 - find a way to highlight canonical data once we have enough of that


Hint: remember that ring moves faster than carrier without thinking about the sun to ring ratio on fixed carrier, because when everything moves at the speed of the sun, we get c' = s and r' = r + s, so r' > c' just because r > 0.


