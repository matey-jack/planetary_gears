import 'package:angular/angular.dart';

import 'package:planetary_gears/app_component.dart';

void main() {
  bootstrap(AppComponent);
}
